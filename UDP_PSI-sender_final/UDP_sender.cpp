#pragma comment(lib, "ws2_32.lib")
#include "stdafx.h"
#include <winsock2.h>
#include "ws2tcpip.h"
#include <windows.security.cryptography.h>

#define TARGET_IP "147.32.221.24"
//147.32.89.11
#define POLY 0x82f63b78
#define BUFFERS_LEN 1024

#define TARGET_PORT 5555
#define LOCAL_PORT 8888
#define SHA1HANDSOFF

#include <stdio.h>
#include <string.h>

/* for uint32_t */
#include <stdint.h>

#include "sha1.h"


#define rol(value, bits) (((value) << (bits)) | ((value) >> (32 - (bits))))

/* blk0() and blk() perform the initial expand. */
/* I got the idea of expanding during the round function from SSLeay */
#if BYTE_ORDER == LITTLE_ENDIAN
#define blk0(i) (block->l[i] = (rol(block->l[i],24)&0xFF00FF00) \
    |(rol(block->l[i],8)&0x00FF00FF))
#elif BYTE_ORDER == BIG_ENDIAN
#define blk0(i) block->l[i]
#else
#error "Endianness not defined!"
#endif
#define blk(i) (block->l[i&15] = rol(block->l[(i+13)&15]^block->l[(i+8)&15] \
    ^block->l[(i+2)&15]^block->l[i&15],1))

/* (R0+R1), R2, R3, R4 are the different operations used in SHA1 */
#define R0(v,w,x,y,z,i) z+=((w&(x^y))^y)+blk0(i)+0x5A827999+rol(v,5);w=rol(w,30);
#define R1(v,w,x,y,z,i) z+=((w&(x^y))^y)+blk(i)+0x5A827999+rol(v,5);w=rol(w,30);
#define R2(v,w,x,y,z,i) z+=(w^x^y)+blk(i)+0x6ED9EBA1+rol(v,5);w=rol(w,30);
#define R3(v,w,x,y,z,i) z+=(((w|x)&y)|(w&x))+blk(i)+0x8F1BBCDC+rol(v,5);w=rol(w,30);
#define R4(v,w,x,y,z,i) z+=(w^x^y)+blk(i)+0xCA62C1D6+rol(v,5);w=rol(w,30);


/* Hash a single 512-bit block. This is the core of the algorithm. */

void SHA1Transform(
	uint32_t state[5],
	const unsigned char buffer[64]
)
{
	uint32_t a, b, c, d, e;

	typedef union
	{
		unsigned char c[64];
		uint32_t l[16];
	} CHAR64LONG16;

#ifdef SHA1HANDSOFF
	CHAR64LONG16 block[1];      /* use array to appear as a pointer */

	memcpy(block, buffer, 64);
#else
	/* The following had better never be used because it causes the
	* pointer-to-const buffer to be cast into a pointer to non-const.
	* And the result is written through.  I threw a "const" in, hoping
	* this will cause a diagnostic.
	*/
	CHAR64LONG16 *block = (const CHAR64LONG16 *)buffer;
#endif
	/* Copy context->state[] to working vars */
	a = state[0];
	b = state[1];
	c = state[2];
	d = state[3];
	e = state[4];
	/* 4 rounds of 20 operations each. Loop unrolled. */
	R0(a, b, c, d, e, 0);
	R0(e, a, b, c, d, 1);
	R0(d, e, a, b, c, 2);
	R0(c, d, e, a, b, 3);
	R0(b, c, d, e, a, 4);
	R0(a, b, c, d, e, 5);
	R0(e, a, b, c, d, 6);
	R0(d, e, a, b, c, 7);
	R0(c, d, e, a, b, 8);
	R0(b, c, d, e, a, 9);
	R0(a, b, c, d, e, 10);
	R0(e, a, b, c, d, 11);
	R0(d, e, a, b, c, 12);
	R0(c, d, e, a, b, 13);
	R0(b, c, d, e, a, 14);
	R0(a, b, c, d, e, 15);
	R1(e, a, b, c, d, 16);
	R1(d, e, a, b, c, 17);
	R1(c, d, e, a, b, 18);
	R1(b, c, d, e, a, 19);
	R2(a, b, c, d, e, 20);
	R2(e, a, b, c, d, 21);
	R2(d, e, a, b, c, 22);
	R2(c, d, e, a, b, 23);
	R2(b, c, d, e, a, 24);
	R2(a, b, c, d, e, 25);
	R2(e, a, b, c, d, 26);
	R2(d, e, a, b, c, 27);
	R2(c, d, e, a, b, 28);
	R2(b, c, d, e, a, 29);
	R2(a, b, c, d, e, 30);
	R2(e, a, b, c, d, 31);
	R2(d, e, a, b, c, 32);
	R2(c, d, e, a, b, 33);
	R2(b, c, d, e, a, 34);
	R2(a, b, c, d, e, 35);
	R2(e, a, b, c, d, 36);
	R2(d, e, a, b, c, 37);
	R2(c, d, e, a, b, 38);
	R2(b, c, d, e, a, 39);
	R3(a, b, c, d, e, 40);
	R3(e, a, b, c, d, 41);
	R3(d, e, a, b, c, 42);
	R3(c, d, e, a, b, 43);
	R3(b, c, d, e, a, 44);
	R3(a, b, c, d, e, 45);
	R3(e, a, b, c, d, 46);
	R3(d, e, a, b, c, 47);
	R3(c, d, e, a, b, 48);
	R3(b, c, d, e, a, 49);
	R3(a, b, c, d, e, 50);
	R3(e, a, b, c, d, 51);
	R3(d, e, a, b, c, 52);
	R3(c, d, e, a, b, 53);
	R3(b, c, d, e, a, 54);
	R3(a, b, c, d, e, 55);
	R3(e, a, b, c, d, 56);
	R3(d, e, a, b, c, 57);
	R3(c, d, e, a, b, 58);
	R3(b, c, d, e, a, 59);
	R4(a, b, c, d, e, 60);
	R4(e, a, b, c, d, 61);
	R4(d, e, a, b, c, 62);
	R4(c, d, e, a, b, 63);
	R4(b, c, d, e, a, 64);
	R4(a, b, c, d, e, 65);
	R4(e, a, b, c, d, 66);
	R4(d, e, a, b, c, 67);
	R4(c, d, e, a, b, 68);
	R4(b, c, d, e, a, 69);
	R4(a, b, c, d, e, 70);
	R4(e, a, b, c, d, 71);
	R4(d, e, a, b, c, 72);
	R4(c, d, e, a, b, 73);
	R4(b, c, d, e, a, 74);
	R4(a, b, c, d, e, 75);
	R4(e, a, b, c, d, 76);
	R4(d, e, a, b, c, 77);
	R4(c, d, e, a, b, 78);
	R4(b, c, d, e, a, 79);
	/* Add the working vars back into context.state[] */
	state[0] += a;
	state[1] += b;
	state[2] += c;
	state[3] += d;
	state[4] += e;
	/* Wipe variables */
	a = b = c = d = e = 0;
#ifdef SHA1HANDSOFF
	memset(block, '\0', sizeof(block));
#endif
}
/* SHA1Init - Initialize new context */

void SHA1Init(
	SHA1_CTX * context
)
{
	/* SHA1 initialization constants */
	context->state[0] = 0x67452301;
	context->state[1] = 0xEFCDAB89;
	context->state[2] = 0x98BADCFE;
	context->state[3] = 0x10325476;
	context->state[4] = 0xC3D2E1F0;
	context->count[0] = context->count[1] = 0;
}


/* Run your data through this. */

void SHA1Update(
	SHA1_CTX * context,
	const unsigned char *data,
	uint32_t len
)
{
	uint32_t i;

	uint32_t j;

	j = context->count[0];
	if ((context->count[0] += len << 3) < j)
		context->count[1]++;
	context->count[1] += (len >> 29);
	j = (j >> 3) & 63;
	if ((j + len) > 63)
	{
		memcpy(&context->buffer[j], data, (i = 64 - j));
		SHA1Transform(context->state, context->buffer);
		for (; i + 63 < len; i += 64)
		{
			SHA1Transform(context->state, &data[i]);
		}
		j = 0;
	}
	else
		i = 0;
	memcpy(&context->buffer[j], &data[i], len - i);
}


/* Add padding and return the message digest. */

void SHA1Final(
	unsigned char digest[20],
	SHA1_CTX * context
)
{
	unsigned i;

	unsigned char finalcount[8];

	unsigned char c;

#if 0    /* untested "improvement" by DHR */
	/* Convert context->count to a sequence of bytes
	* in finalcount.  Second element first, but
	* big-endian order within element.
	* But we do it all backwards.
	*/
	unsigned char *fcp = &finalcount[8];

	for (i = 0; i < 2; i++)
	{
		uint32_t t = context->count[i];

		int j;

		for (j = 0; j < 4; t >>= 8, j++)
			*--fcp = (unsigned char)t
	}
#else
	for (i = 0; i < 8; i++)
	{
		finalcount[i] = (unsigned char)((context->count[(i >= 4 ? 0 : 1)] >> ((3 - (i & 3)) * 8)) & 255);      /* Endian independent */
	}
#endif
	c = 0200;
	SHA1Update(context, &c, 1);
	while ((context->count[0] & 504) != 448)
	{
		c = 0000;
		SHA1Update(context, &c, 1);
	}
	SHA1Update(context, finalcount, 8); /* Should cause a SHA1Transform() */
	for (i = 0; i < 20; i++)
	{
		digest[i] = (unsigned char)
			((context->state[i >> 2] >> ((3 - (i & 3)) * 8)) & 255);
	}
	/* Wipe variables */
	memset(context, '\0', sizeof(*context));
	memset(&finalcount, '\0', sizeof(finalcount));
}

void SHA1(
	char *hash_out,
	const char *str,
	int len)
{
	SHA1_CTX ctx;
	unsigned int ii;

	SHA1Init(&ctx);
	for (ii = 0; ii<len; ii += 1)
		SHA1Update(&ctx, (const unsigned char*)str + ii, 1);
	SHA1Final((unsigned char *)hash_out, &ctx);
	hash_out[20] = '\0';
}

void InitWinsock()
{
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);
}

uint32_t crc32c(uint32_t crc, const unsigned char *buf, size_t len)
{
	int k;

	crc = ~crc;
	while (len--) {
		crc ^= *buf++;
		for (k = 0; k < 8; k++)
			crc = crc & 1 ? (crc >> 1) ^ POLY : crc >> 1;
	}

	return ~crc;
}

//**********************************************************************
int main(int argc, char* argv[])
{
START:
	FILE* file = fopen(argv[1], "rb");

	if (!file) {
		perror("ERROR: No file specified!\n");
	}

	fseek(file, 0, SEEK_END);
	int flen = ftell(file);

	rewind(file);

	printf("Filename: %s\n", argv[1]);
	printf("File size: %d\n", flen);

	SOCKET socketS;

	InitWinsock();
	uint32_t crc = 0;
	struct sockaddr_in local;
	struct sockaddr_in from;

	int fromlen = sizeof(from);
	local.sin_family = AF_INET;
	local.sin_port = htons(LOCAL_PORT);
	local.sin_addr.s_addr = INADDR_ANY;

	socketS = socket(AF_INET, SOCK_DGRAM, 0);
	if (bind(socketS, (sockaddr*)&local, sizeof(local)) != 0) {
		printf("Binding error!\n");
		getchar(); //wait for press Enter
		return 1;
	}

	char* buffer_tx = (char *)malloc(1024);
	char buffer_rx[sizeof(char) + sizeof(float)];
	float speed;

	sockaddr_in addrDest;
	addrDest.sin_family = AF_INET;
	addrDest.sin_port = htons(TARGET_PORT);
	InetPton(AF_INET, _T(TARGET_IP), &addrDest.sin_addr.s_addr);

	int time = 1000;

	setsockopt(socketS, SOL_SOCKET, SO_RCVTIMEO, (char*)&time, sizeof(int));

	printf("Starting transmission...\n");


	memcpy(buffer_tx, &flen, sizeof(int));
	strcpy((buffer_tx + 4), argv[1]);
	crc = crc32c(0, (unsigned char*)(buffer_tx + 4), 1016);
	memcpy((buffer_tx + 1020), &crc, sizeof(uint32_t));


	for (;;) {
		sendto(socketS, buffer_tx, 1024, 0, (sockaddr*)&addrDest, sizeof(addrDest));

		if (recvfrom(socketS, buffer_rx, sizeof(char) + sizeof(float), 0, (sockaddr*)&from, &fromlen) < 0) {
			sendto(socketS, buffer_tx, 1024, 0, (sockaddr*)&addrDest, sizeof(addrDest));
			fprintf(stderr, "Header packet confirmation: timeout!\n");
		}
		else {
			if (buffer_rx[0] != 'y') {
				fprintf(stderr, "ERROR: Corrupted packet detected! \n");
			}
			else {
				memcpy(&speed, (buffer_rx + sizeof(char)), sizeof(float));
				printf("Header arrival confirmed!\nMAX SPEED: %f kB/s\n", speed);
				break;
			}
		}
	}

	int packet_count = ((flen - (flen % 1016)) / 1016);
	printf("PACKET COUNT: %d\n", packet_count);

	Sleep(60);

	float delay = 1000 / speed;

	for (int i = 0; i < packet_count; i++) {
		memcpy(buffer_tx, &i, 4);
		fread((buffer_tx + 4), 1016, 1, file);

		crc = crc32c(0, (unsigned char*)(buffer_tx + 4), 1016);

		memcpy((buffer_tx + 1020), &crc, sizeof(uint32_t));

		sendto(socketS, buffer_tx, 1024, 0, (sockaddr*)&addrDest, sizeof(addrDest));


		for (;;) {
			if (recvfrom(socketS, buffer_rx, 1, 0, (sockaddr*)&from, &fromlen) < 0) {
				fprintf(stderr, "TIMEOUT!\nPacket. no:%d\n", i);
				sendto(socketS, buffer_tx, 1024, 0, (sockaddr*)&addrDest, sizeof(addrDest));
				continue;
			}

			if (buffer_rx[0] == 'y') {
				break;
			}
			else {
				fprintf(stderr, "ERROR: Corrupted packet detected! \n");
				printf("RESPONSE IS: %c\n", buffer_rx[0]);
				printf("CRC IS: %d\n", crc);
				sendto(socketS, buffer_tx, 1024, 0, (sockaddr*)&addrDest, sizeof(addrDest));

			}

		}
		Sleep(delay);
	}

	memcpy(buffer_tx, &packet_count, 4);
	fread((buffer_tx + 4), (flen % 1016), 1, file);

	for (int i = (flen % 1016) + 4; i < 1020; ++i) {
		buffer_tx[i] = '0';
	}

	crc = crc32c(0, (unsigned char*)(buffer_tx + 4), 1016);

	memcpy((buffer_tx + 1020), &crc, sizeof(uint32_t));


	for (;;) {
		sendto(socketS, buffer_tx, 1024, 0, (sockaddr*)&addrDest, sizeof(addrDest));

		if (recvfrom(socketS, buffer_rx, 1, 0, (sockaddr*)&from, &fromlen) < 0) {
			fprintf(stderr, "Timeout!\n");
		}
		else {
			if (buffer_rx[0] != 'y') {
				fprintf(stderr, "ERROR: Corrupted packet detected! \n");
			}
			else {
				printf("Last packet confirmed!\n");
				break;
			}
		}
	}
	char hash[21];

	rewind(file);
	char* f = (char *)malloc(flen);
	fread(f, 1, flen, file);
	SHA1(hash, f, flen);

	for (;;) {
		sendto(socketS, (char*)hash, 20, 0, (sockaddr*)&addrDest, sizeof(addrDest));

		if (recvfrom(socketS, buffer_rx, 1, 0, (sockaddr*)&from, &fromlen) < 0) {
			sendto(socketS, buffer_tx, 1024, 0, (sockaddr*)&addrDest, sizeof(addrDest));
			fprintf(stderr, "Hash packet confirmation: timeout!\n");
		}
		else {
			if (buffer_rx[0] != 'y') {
				fprintf(stderr, "ERROR: Corrupted packet detected! \n");
			}
			else {
				printf("Hash arrival confirmed!\n");
				break;
			}
		}
	}

	closesocket(socketS);
	free(buffer_tx);
	free(f);


	printf("REPEAT? y/n\n");

	if (getchar() == 'y') {
		goto START;
	}

	return 0;
}
