#pragma comment(lib, "ws2_32.lib")
#include "stdafx.h"
#include <winsock2.h>
#include "ws2tcpip.h"
#include <windows.security.cryptography.h>

#define TARGET_IP "147.32.216.212"
//147.32.89.11
#define POLY 0x82f63b78
#define BUFFERS_LEN 1024

#define TARGET_PORT 5555
#define LOCAL_PORT 8888
#define SHA1HANDSOFF

#include <stdio.h>
#include <string.h>

/* for uint32_t */
#include <stdint.h>

#include "sha1.h"


#define rol(value, bits) (((value) << (bits)) | ((value) >> (32 - (bits))))

/* blk0() and blk() perform the initial expand. */
/* I got the idea of expanding during the round function from SSLeay */
#if BYTE_ORDER == LITTLE_ENDIAN
#define blk0(i) (block->l[i] = (rol(block->l[i],24)&0xFF00FF00) \
    |(rol(block->l[i],8)&0x00FF00FF))
#elif BYTE_ORDER == BIG_ENDIAN
#define blk0(i) block->l[i]
#else
#error "Endianness not defined!"
#endif
#define blk(i) (block->l[i&15] = rol(block->l[(i+13)&15]^block->l[(i+8)&15] \
    ^block->l[(i+2)&15]^block->l[i&15],1))

/* (R0+R1), R2, R3, R4 are the different operations used in SHA1 */
#define R0(v,w,x,y,z,i) z+=((w&(x^y))^y)+blk0(i)+0x5A827999+rol(v,5);w=rol(w,30);
#define R1(v,w,x,y,z,i) z+=((w&(x^y))^y)+blk(i)+0x5A827999+rol(v,5);w=rol(w,30);
#define R2(v,w,x,y,z,i) z+=(w^x^y)+blk(i)+0x6ED9EBA1+rol(v,5);w=rol(w,30);
#define R3(v,w,x,y,z,i) z+=(((w|x)&y)|(w&x))+blk(i)+0x8F1BBCDC+rol(v,5);w=rol(w,30);
#define R4(v,w,x,y,z,i) z+=(w^x^y)+blk(i)+0xCA62C1D6+rol(v,5);w=rol(w,30);


/* Hash a single 512-bit block. This is the core of the algorithm. */

void SHA1Transform(
	uint32_t state[5],
	const unsigned char buffer[64]
)
{
	uint32_t a, b, c, d, e;

	typedef union
	{
		unsigned char c[64];
		uint32_t l[16];
	} CHAR64LONG16;

#ifdef SHA1HANDSOFF
	CHAR64LONG16 block[1];      /* use array to appear as a pointer */

	memcpy(block, buffer, 64);
#else
	/* The following had better never be used because it causes the
	* pointer-to-const buffer to be cast into a pointer to non-const.
	* And the result is written through.  I threw a "const" in, hoping
	* this will cause a diagnostic.
	*/
	CHAR64LONG16 *block = (const CHAR64LONG16 *)buffer;
#endif
	/* Copy context->state[] to working vars */
	a = state[0];
	b = state[1];
	c = state[2];
	d = state[3];
	e = state[4];
	/* 4 rounds of 20 operations each. Loop unrolled. */
	R0(a, b, c, d, e, 0);
	R0(e, a, b, c, d, 1);
	R0(d, e, a, b, c, 2);
	R0(c, d, e, a, b, 3);
	R0(b, c, d, e, a, 4);
	R0(a, b, c, d, e, 5);
	R0(e, a, b, c, d, 6);
	R0(d, e, a, b, c, 7);
	R0(c, d, e, a, b, 8);
	R0(b, c, d, e, a, 9);
	R0(a, b, c, d, e, 10);
	R0(e, a, b, c, d, 11);
	R0(d, e, a, b, c, 12);
	R0(c, d, e, a, b, 13);
	R0(b, c, d, e, a, 14);
	R0(a, b, c, d, e, 15);
	R1(e, a, b, c, d, 16);
	R1(d, e, a, b, c, 17);
	R1(c, d, e, a, b, 18);
	R1(b, c, d, e, a, 19);
	R2(a, b, c, d, e, 20);
	R2(e, a, b, c, d, 21);
	R2(d, e, a, b, c, 22);
	R2(c, d, e, a, b, 23);
	R2(b, c, d, e, a, 24);
	R2(a, b, c, d, e, 25);
	R2(e, a, b, c, d, 26);
	R2(d, e, a, b, c, 27);
	R2(c, d, e, a, b, 28);
	R2(b, c, d, e, a, 29);
	R2(a, b, c, d, e, 30);
	R2(e, a, b, c, d, 31);
	R2(d, e, a, b, c, 32);
	R2(c, d, e, a, b, 33);
	R2(b, c, d, e, a, 34);
	R2(a, b, c, d, e, 35);
	R2(e, a, b, c, d, 36);
	R2(d, e, a, b, c, 37);
	R2(c, d, e, a, b, 38);
	R2(b, c, d, e, a, 39);
	R3(a, b, c, d, e, 40);
	R3(e, a, b, c, d, 41);
	R3(d, e, a, b, c, 42);
	R3(c, d, e, a, b, 43);
	R3(b, c, d, e, a, 44);
	R3(a, b, c, d, e, 45);
	R3(e, a, b, c, d, 46);
	R3(d, e, a, b, c, 47);
	R3(c, d, e, a, b, 48);
	R3(b, c, d, e, a, 49);
	R3(a, b, c, d, e, 50);
	R3(e, a, b, c, d, 51);
	R3(d, e, a, b, c, 52);
	R3(c, d, e, a, b, 53);
	R3(b, c, d, e, a, 54);
	R3(a, b, c, d, e, 55);
	R3(e, a, b, c, d, 56);
	R3(d, e, a, b, c, 57);
	R3(c, d, e, a, b, 58);
	R3(b, c, d, e, a, 59);
	R4(a, b, c, d, e, 60);
	R4(e, a, b, c, d, 61);
	R4(d, e, a, b, c, 62);
	R4(c, d, e, a, b, 63);
	R4(b, c, d, e, a, 64);
	R4(a, b, c, d, e, 65);
	R4(e, a, b, c, d, 66);
	R4(d, e, a, b, c, 67);
	R4(c, d, e, a, b, 68);
	R4(b, c, d, e, a, 69);
	R4(a, b, c, d, e, 70);
	R4(e, a, b, c, d, 71);
	R4(d, e, a, b, c, 72);
	R4(c, d, e, a, b, 73);
	R4(b, c, d, e, a, 74);
	R4(a, b, c, d, e, 75);
	R4(e, a, b, c, d, 76);
	R4(d, e, a, b, c, 77);
	R4(c, d, e, a, b, 78);
	R4(b, c, d, e, a, 79);
	/* Add the working vars back into context.state[] */
	state[0] += a;
	state[1] += b;
	state[2] += c;
	state[3] += d;
	state[4] += e;
	/* Wipe variables */
	a = b = c = d = e = 0;
#ifdef SHA1HANDSOFF
	memset(block, '\0', sizeof(block));
#endif
}
/* SHA1Init - Initialize new context */

void SHA1Init(
	SHA1_CTX * context
)
{
	/* SHA1 initialization constants */
	context->state[0] = 0x67452301;
	context->state[1] = 0xEFCDAB89;
	context->state[2] = 0x98BADCFE;
	context->state[3] = 0x10325476;
	context->state[4] = 0xC3D2E1F0;
	context->count[0] = context->count[1] = 0;
}


/* Run your data through this. */

void SHA1Update(
	SHA1_CTX * context,
	const unsigned char *data,
	uint32_t len
)
{
	uint32_t i;

	uint32_t j;

	j = context->count[0];
	if ((context->count[0] += len << 3) < j)
		context->count[1]++;
	context->count[1] += (len >> 29);
	j = (j >> 3) & 63;
	if ((j + len) > 63)
	{
		memcpy(&context->buffer[j], data, (i = 64 - j));
		SHA1Transform(context->state, context->buffer);
		for (; i + 63 < len; i += 64)
		{
			SHA1Transform(context->state, &data[i]);
		}
		j = 0;
	}
	else
		i = 0;
	memcpy(&context->buffer[j], &data[i], len - i);
}


/* Add padding and return the message digest. */

void SHA1Final(
	unsigned char digest[20],
	SHA1_CTX * context
)
{
	unsigned i;

	unsigned char finalcount[8];

	unsigned char c;

#if 0    /* untested "improvement" by DHR */
	/* Convert context->count to a sequence of bytes
	* in finalcount.  Second element first, but
	* big-endian order within element.
	* But we do it all backwards.
	*/
	unsigned char *fcp = &finalcount[8];

	for (i = 0; i < 2; i++)
	{
		uint32_t t = context->count[i];

		int j;

		for (j = 0; j < 4; t >>= 8, j++)
			*--fcp = (unsigned char)t
	}
#else
	for (i = 0; i < 8; i++)
	{
		finalcount[i] = (unsigned char)((context->count[(i >= 4 ? 0 : 1)] >> ((3 - (i & 3)) * 8)) & 255);      /* Endian independent */
	}
#endif
	c = 0200;
	SHA1Update(context, &c, 1);
	while ((context->count[0] & 504) != 448)
	{
		c = 0000;
		SHA1Update(context, &c, 1);
	}
	SHA1Update(context, finalcount, 8); /* Should cause a SHA1Transform() */
	for (i = 0; i < 20; i++)
	{
		digest[i] = (unsigned char)
			((context->state[i >> 2] >> ((3 - (i & 3)) * 8)) & 255);
	}
	/* Wipe variables */
	memset(context, '\0', sizeof(*context));
	memset(&finalcount, '\0', sizeof(finalcount));
}

void SHA1(
	char *hash_out,
	const char *str,
	unsigned int len)
{
	SHA1_CTX ctx;
	unsigned int ii;

	SHA1Init(&ctx);
	for (ii = 0; ii<len; ii += 1)
		SHA1Update(&ctx, (const unsigned char*)str + ii, 1);
	SHA1Final((unsigned char *)hash_out, &ctx);
	hash_out[20] = '\0';
}

void InitWinsock()
{
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);
}

uint32_t crc32c(uint32_t crc, const unsigned char *buf, size_t len)
{
	int k;

	crc = ~crc;
	while (len--) {
		crc ^= *buf++;
		for (k = 0; k < 8; k++)
			crc = crc & 1 ? (crc >> 1) ^ POLY : crc >> 1;
	}

	return ~crc;
}

//**********************************************************************
int main()
{
START:
	SOCKET socketS;
	InitWinsock();
	struct sockaddr_in local;
	struct sockaddr_in from;
	int fromlen = sizeof(from);
	local.sin_family = AF_INET;
	local.sin_port = htons(LOCAL_PORT);
	local.sin_addr.s_addr = INADDR_ANY;
	socketS = socket(AF_INET, SOCK_DGRAM, 0);

	if (bind(socketS, (sockaddr*)&local, sizeof(local)) != 0) {
		printf("Binding error!\n");
		getchar(); //wait for press Enter
		return 1;
	}

	sockaddr_in addrDest;
	addrDest.sin_family = AF_INET;
	addrDest.sin_port = htons(TARGET_PORT);
	InetPton(AF_INET, _T(TARGET_IP), &addrDest.sin_addr.s_addr);

	char buffer_rx[BUFFERS_LEN];
	char buffer_tx[sizeof(char) + sizeof(float)];
	char buffer_sockets[BUFFERS_LEN];
	char* fname;
	int size;
	int num, rcv_crc, q = 0;
	float speed;


	strncpy_s(buffer_rx, "No data received.\n", BUFFERS_LEN);
	printf("*!PROGRAM STARTED!*\n");		//program started

											////////////////////////////////////////////////////// *SPEED* ///////////////////////////////////////////////////
SPEED:
	printf("Enter maximal speed (in kb/s): ");	//maximal speed to sender
	scanf_s("%f", &speed);
	if (speed > 0) {
		printf("\nThe speed si %f\n", speed);
	}
	else {
		printf("\nERROR: You have to enter speed\n");
		goto SPEED;
	}
	////////////////////////////////////////////////////// END SPEED ///////////////////////////////////////////////////

	strcpy(buffer_tx, "y");
	memcpy(buffer_tx + sizeof(char), &speed, sizeof(float));	//buffer_tx contains y and speed number
	printf("Waiting for datagram ...\n");
	uint32_t crc = 0;

	////////////////////////////////////////////////////// HEADER ///////////////////////////////////////////////////
	//Receive information about file
HEADER:
	if (recvfrom(socketS, buffer_rx, sizeof(buffer_rx), 0, (sockaddr*)&from, &fromlen) == SOCKET_ERROR) {
		printf("Socket error!\n");
		getchar();
		return 1;
	}
	else {
		memcpy(&num, (buffer_rx + 1020), sizeof(uint32_t));


		crc = crc32c(0, (unsigned char*)(buffer_rx + 4), 1016);
		printf("CRC : calculated | %d %d\n", num, crc);
		getchar();
		if (crc == num) {
			sendto(socketS, buffer_tx, sizeof(char) + sizeof(float), 0, (sockaddr*)&addrDest, sizeof(addrDest));
			memcpy(&size, buffer_rx, sizeof(int));
			fname = (char *)malloc(strlen((buffer_rx + 4)));
			strcpy(fname, (buffer_rx + 4));
		}
		else {
			sendto(socketS, "n", 1, 0, (sockaddr*)&addrDest, sizeof(addrDest));
			printf("WRONG HEADER\n");

			goto HEADER;
		}
	}
	printf("Header recieved!\n");

	////////////////////////////////////////////////////// END HEADER ///////////////////////////////////////////////////


	FILE* file = fopen(fname, "wb");			//open file from header

	int packet_count = ((size - (size % 1016)) / 1016) + 1;
	printf("Packets count %d\n", packet_count);
	bool cond = false;
	char* arrayof = (char *)malloc(packet_count * 1016 + 1016);
	char response;
	int i = 0;
	char* packet_stat = (char *)malloc(packet_count);
	////////////////////////////////////////////////////// RECIEVE PACKETS ///////////////////////////////////////////////////

	for (;;) {
		if (i == packet_count) {

			break;
		}
		recvfrom(socketS, buffer_rx, sizeof(buffer_rx), 0, (sockaddr*)&from, &fromlen);
		memcpy(&num, buffer_rx, sizeof(int));
		printf("Packet no.: %d\n", num);
		memcpy(&rcv_crc, (buffer_rx + 1020), sizeof(uint32_t));
		crc = crc32c(0, (unsigned char*)(buffer_rx + 4), 1016);

		if (crc == rcv_crc) {
			if (packet_stat[num] != 'y') {
				++i;
			}
			packet_stat[num] = 'y';
			memcpy(arrayof + (num * 1016), buffer_rx + 4, 1016);
			strcpy(buffer_tx, "y");
			memcpy(buffer_tx + sizeof(char), &num, sizeof(int));
			printf("Packet no.: %d has been written\n", num);

			sendto(socketS, buffer_tx, sizeof(char) + sizeof(int), 0, (sockaddr*)&addrDest, sizeof(addrDest));
		}
		else {
			int nic = 5;
			strcpy(buffer_tx, "n");
			memcpy(buffer_tx + sizeof(char), &nic, sizeof(int));
			sendto(socketS, buffer_tx, 5, 0, (sockaddr*)&addrDest, sizeof(addrDest));
			printf("%d : %d\n", crc, rcv_crc);
			fprintf(stderr, "ERROR: Corrupted packet detected! 2\n");
		}
	}
	////////////////////////////////////////////////////// END RECIEVE PACKETSK ///////////////////////////////////////////////////	

	////////////////////////////////////////////////////// *RECIEVE HASH* ///////////////////////////////////////////////////

	char hash[21];
	fwrite(arrayof, 1016, packet_count - 1, file);
	fwrite(arrayof + ((packet_count - 1) * 1016), size % 1016, 1, file);
	fclose(file);
	file = fopen(fname, "rb");
	char* f = (char *)malloc(size);
	fread(f, 1, size, file);
	SHA1(hash, f, size);
HASH:/*
	 if (recvfrom(socketS, buffer_rx, 20, 0, (sockaddr*)&from, &fromlen) == SOCKET_ERROR) {
	 printf("Socket error!\n");
	 fwrite(arrayof, 1016, packet_count - 1, file);
	 fwrite(arrayof + (size - (size % 1016)), size % 1016, 1, file);
	 fclose(file);
	 getchar();
	 //return 1;
	 }
	 else {*/
	recvfrom(socketS, buffer_rx, 20, 0, (sockaddr*)&from, &fromlen);
	if (strncmp(hash, (buffer_rx), 20) == 0) {
		sendto(socketS, "y", 1, 0, (sockaddr*)&addrDest, sizeof(addrDest));
		printf("Hash is same\n");
		for (int i = 0; i < 20; i++)
		{
			printf("%c", hash[i]);
		}
		printf(" : ");
		for (int i = 0; i < 20; i++)
		{
			printf("%c", buffer_rx[i]);
		}
		printf("\n");
	}
	else {
		printf("Data stream overflow, sending confirmation!\n");

		strcpy(buffer_tx, "y");

		int dia;

		memcpy(&dia, buffer_rx, 4);
		memcpy(buffer_tx + sizeof(char), &dia, sizeof(int));

		sendto(socketS, buffer_tx, sizeof(char) + sizeof(int), 0, (sockaddr*)&addrDest, sizeof(addrDest));


		memcpy(&dia, buffer_rx, 4);
		printf("%d\n", dia);
		Sleep(1000);
		goto HASH;
	}

	////////////////////////////////////////////////////// END HASH ///////////////////////////////////////////////////
	////////////////////////////////////////////////////// END CHECK HASH ///////////////////////////////////////////////////

	printf("SUCCES: All packets received.\n");


	closesocket(socketS);

	printf("REPEAT? y/n\n");
	if (getchar() == 'y') {
		goto START;
	}
	getchar();

	return 0;
}
